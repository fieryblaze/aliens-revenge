#ifndef STARTMENU_H
#define STARTMENU_H

#include <iostream>
#include <GL/glut.h>
#include "./Objects/Button.cpp"
#include "./HelperFunctions/Texture.cpp"
#include "./HelperFunctions/CheckCollision.cpp"
#include "./Globals.cpp"

class StartMenu{
private:
	StartMenu();

	static bool instanceFlag;
	static StartMenu* sMenu;

	int MousePosX;
	int MousePosY;
	
	int state;

	Texture* title;
	Button* cont;
	Button* newGame;
	Button* exit; 
public:
	static StartMenu* getInstance();

	~StartMenu(){instanceFlag = false;}

	void display();
	void mousePress(float x, float y);
	void mouseUp();
};

StartMenu* sMenu;

#endif
