OP=./Objects/
HP=./HelperFunctions/
OBJECTS=$(OP)Button.cpp
HELPERS=$(HP)Rect.cpp $(HP)Texture.cpp $(HP)CheckCollision.cpp
SOURCES=AliensRevenge.cpp StartMenu.cpp GlutFunctions.cpp Globals.cpp

AliensRevenge: $(SOURCES) $(HELPERS) $(OBJECTS) 
	g++ -Wall AliensRevenge.cpp -lGL -lGLU -lglut -lSOIL -o $@

remove:
	rm -f *~ $(OP)*~ $(HP)*~

install:
	make AliensRevenge
	make remove
	
clean:
	make remove
	rm -f AliensRevenge
