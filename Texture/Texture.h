#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glut.h>
#include <SOIL/SOIL.h>
#include <fstream>

class Texture{
private:
	GLuint texture;
public:
	Texture(const char* filename);
	GLuint getTexture();
};

#endif
