#ifndef STARTMENU_CPP
#define STARTMENU_CPP

#include "StartMenu.h"

#define TITLE "./Images/Title.png"
#define CONTPRESSED "./Images/ContinuePressed.png"
#define CONTUNPRESSED "./Images/ContinueUnpressed.png"
#define NEWGAMEPRESSED "./Images/NewGamePressed.png"
#define NEWGAMEUNPRESSED "./Images/NewGameUnpressed.png"
#define EXITPRESSED "./Images/ExitPressed.png"
#define EXITUNPRESSED "./Images/ExitUnpressed.png"

#define CONTINUE 1
#define NEWGAME 2
#define EXIT 3

void ng(void){std::cout << "New Game\n";}
void con(void){std::cout << "Continue\n";}
void ex(void){exit(0);}

bool StartMenu::instanceFlag = false;
StartMenu* StartMenu::sMenu = NULL;

StartMenu::StartMenu(){
	////title = new Texture(TITLE);
	std::cout << "Creating StartMenu\n";
	instanceFlag = false;
	title = new Texture(TITLE);
	//sMenu = NULL;

	Rect<float>* rec = new Rect<float>(-20.0,15.0, 40.0,10.0);
	cont = new Button(NEWGAMEPRESSED, NEWGAMEUNPRESSED, rec, con);
	
	rec = new Rect<float>(-20.0,5.0, 40.0,10.0);
	newGame = new Button(NEWGAMEPRESSED, NEWGAMEUNPRESSED, rec, ng);
	
	rec = new Rect<float>(-20.0,-5.0, 40.0,10.0);
	exit = new Button(NEWGAMEPRESSED, NEWGAMEUNPRESSED, rec, ex);
	
}

StartMenu* StartMenu::getInstance(){
	if(! instanceFlag){
		sMenu = new StartMenu();
		instanceFlag = true;
	}
	return sMenu;
}

void StartMenu::display(){
	glBindTexture(GL_TEXTURE_2D, title->getTexture());
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(-screenWidth, -screenHeight);
		glTexCoord2f(0.0, 1.0); glVertex2f(-screenWidth, screenHeight);
		glTexCoord2f(1.0, 1.0); glVertex2f(screenWidth, screenHeight);
		glTexCoord2f(1.0, 0.0); glVertex2f(screenWidth, -screenHeight);
   glEnd();
   
	newGame->display();
	cont->display();
	exit->display();
}

void StartMenu::mousePress(float x, float y){
	MousePosX = x;
	MousePosY = y;
	if(checkCollision(x,y,*newGame->getRect())){
		newGame->press();
		state = NEWGAME;
	}
	else if(checkCollision(x,y,*cont->getRect())){
		cont->press();
		state = CONTINUE;
	}
	else if(checkCollision(x,y,*exit->getRect())){
		exit->press();
		state = EXIT;
	}
	else{
		state = 0;
	}
}

void StartMenu::mouseUp(){
	switch(state){
		case NEWGAME:
			newGame->unpress();
			break;
			
		case CONTINUE:
			cont->unpress();
			break;
			
		case EXIT:
			exit->unpress();
			break;
	}
}

#endif
