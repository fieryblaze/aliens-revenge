#include "../src/UnitTest++.h"
#include "../../Rect/Rect.cpp"
#include <iostream>

TEST(Rect)
{
	std::cout << "\nRECT test:\n";
	Rect<float>* rec = new Rect<float>(1.0,2.0,3.0,4.0);
	CHECK_EQUAL(1.0, rec->x());
	CHECK_EQUAL(2.0, rec->y());
	CHECK_EQUAL(3.0, rec->width());
	CHECK_EQUAL(4.0, rec->height());
	
	Rect<char>* recCH = new Rect<char>('c','h','a','r');
	CHECK_EQUAL('c', recCH->x());
}


