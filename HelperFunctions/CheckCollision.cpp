#ifndef CHECKCOLLISION_CPP
#define CHECKCOLLISION_CPP

#include "./Rect.cpp"

bool checkCollision(Rect<float> rect1, Rect<float> rect2){
	float botA = rect1.y() - rect1.height();
	float topA = rect1.y();
	float leftA = rect1.x();
	float rightA = rect1.x() + rect1.width();
	
	float botB = rect2.y() - rect2.height();
	float topB = rect2.y();
	float leftB = rect2.x();
	float rightB = rect2.x() + rect2.width();

	return !((botA >= topB)
			|| (topA <= botB)
			|| (rightA <= leftB)
			|| (leftA >= rightB));
}

bool checkCollision(float x, float y, Rect<float> rect){
	float bot = rect.y() - rect.height();
	float top = rect.y();
	float left = rect.x();
	float right = rect.x() + rect.width();
	
	return !((x <= left)
			|| (x >= right)
			|| (y >= top)
			|| (y <= bot));
}

#endif
