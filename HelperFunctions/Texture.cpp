#ifndef TEXTURE_CPP
#define TEXTURE_CPP

#include "Texture.h"

Texture::Texture(const char* filename){
	std::cout << "Creating texture " << filename << std::endl;
	texture = SOIL_load_OGL_texture
	(	filename,
	   SOIL_LOAD_AUTO,
	   SOIL_CREATE_NEW_ID,
	   SOIL_FLAG_INVERT_Y
	);

	// check for an error during the load process 
	if( 0 == texture )
	{
		printf( "SOIL loading error: %s\n", SOIL_last_result() );
	}

}

GLuint Texture::getTexture(){
	return texture;
}

#endif
