#ifndef STARTMENU_CPP
#define STARTMENU_CPP

#include "StartMenu.h"

#define TITLE "./Title.png"
#define CONT "./Continue.png"
#define NEWGAMEPRESSED "./Images/NewGamePressed.png"
#define NEWGAMEUNPRESSED "./Images/NewGameUnpressed.png"
#define EXIT "./Exit.png"

void ng(void){std::cout << "New Game\n";}
void con(void){std::cout << "Continue\n";}
void ex(void){std::cout << "Exit\n";}

bool StartMenu::instanceFlag = false;
StartMenu* StartMenu::sMenu;

StartMenu::StartMenu(){
	////title = new Texture(TITLE);
	std::cout << "Creating StartMenu\n";
	instanceFlag = false;
	sMenu = NULL;
	Rect<float>* rec = new Rect<float>(-20.0,5.0, 40.0,10.0);
	newGame = new Button(NEWGAMEPRESSED, NEWGAMEUNPRESSED, rec, ng);

	rec = new Rect<float>(-20.0,15.0, 40.0,10.0);
	cont = new Button(NEWGAMEPRESSED, NEWGAMEUNPRESSED, rec, con);
	
	rec = new Rect<float>(-20.0,-5.0, 40.0,10.0);
	exit = new Button(NEWGAMEPRESSED, NEWGAMEUNPRESSED, rec, ex);
	
}

StartMenu* StartMenu::getInstance(){
	if(! instanceFlag){
		sMenu = new StartMenu();
		instanceFlag = true;
	}
	return sMenu;
}

void StartMenu::display(){
	newGame->display();
	cont->display();
	exit->display();/*
	glBindTexture(GL_TEXTURE_2D, title.getTexture());
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(-10.0, -10.0);
		glTexCoord2f(0.0, 10.0); glVertex2f(-10.0, 10.0);
		glTexCoord2f(10.0, 10.0); glVertex2f(10.0, 10.0);
		glTexCoord2f(10.0, 0.0); glVertex2f(10.0, -10.0);
	glEnd();*/
}

#endif
