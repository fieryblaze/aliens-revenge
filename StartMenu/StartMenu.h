#ifndef STARTMENU_H
#define STARTMENU_H

#include <GL/glut.h>
#include "../Button/Button.cpp"
#include "../Texture/Texture.cpp"

class StartMenu{
private:
	StartMenu();

	static bool instanceFlag;
	static StartMenu* sMenu;

	Texture* title;
	Button* cont;
	Button* newGame;
	Button* exit; 
public:
	static StartMenu* getInstance();

	~StartMenu(){instanceFlag = false;}

	void display();
};

#endif
