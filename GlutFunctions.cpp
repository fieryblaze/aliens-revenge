#ifndef GLUTFUNCTIONS_CPP
#define GLUTFUNCTIONS_CPP

#include "Globals.cpp"

/* Initialize OpenGL Graphics */
void initGL() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	sMenu = StartMenu::getInstance();
}

/* Called back when timer expired */
void timer(int value) {
	glutPostRedisplay();
	if(isPaused == 0)
		glutTimerFunc(refreshMills, timer, 0);
}

/* Handler for window re-size event. Called back when the window first appears 
	and whenever the window is re-sized with its new width and height */
void reshape(GLsizei width, GLsizei height) {
	//  Ratio of the width to the height of the window
   double w2h = (height>0) ? (double)width/height : 1;
   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   screenHeight = dim;
   screenWidth = w2h * dim;
   //  Tell OpenGL we want to manipulate the projection matrix
   glMatrixMode(GL_PROJECTION);
   //  Undo previous transformations
   glLoadIdentity();
   //  Orthogonal projection
   gluOrtho2D(-w2h*dim, +w2h*dim, -dim, +dim);
   //  Switch to manipulating the model matrix
   glMatrixMode(GL_MODELVIEW);
   //  Undo previous transformations
   glLoadIdentity();
}

void specialKeypress(int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_UP:
				camY -= dim/10.0f;
			break;
		case GLUT_KEY_DOWN:
				camY += dim/10.0f;
			break;
		case GLUT_KEY_RIGHT:
				camX -= dim/10;
			break;
		case GLUT_KEY_LEFT:
				camX += dim/10;
			break;
		default:
			break;
	}
}

void key(unsigned char ch,int x,int y)
{
	switch (ch) {
		case 27: /*"ESC"*/
			exit(0);
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	int width = m_viewport[2];
	int height = m_viewport[3];
	float xPos = ((float)x / (float)width) * (2.0 * screenWidth);
	xPos = xPos - screenWidth;
	float yPos = ((float)y / (float)height) * (2.0 * screenHeight);
	yPos = screenHeight - yPos;
	
	switch (button) {
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){
				switch (gameState) {
					case START:
						sMenu->mousePress(xPos, yPos);
						break;
				}
			}
			else{
				switch (gameState) {
					case START:
						sMenu->mouseUp();
						break;
				}
			}
			break;
	}
}

#endif
