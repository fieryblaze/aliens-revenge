#ifndef RECT_CPP
#define RECT_CPP

template <class type>
class Rect{
public:
	Rect(type x, type y, type width, type height){
		xPos = x;
		yPos = y;
		w = width;
		h = height;
	}
	type x(){ return xPos;}
	type y(){ return yPos;}
	type width(){ return w;}
	type height(){ return h;}
	void setXY(type x, type y){ xPos = x; yPos = y;}
	void setWH(type width, type height){ w = width; h = height;}
	
private:
	type xPos;
	type yPos;
	type w;
	type h;
};

#endif
