#ifndef BUTTON_H
#define BUTTON_H

#include <GL/glut.h>
#include "../Texture/Texture.cpp"
#include "../Rect/Rect.cpp"

class Button{
public:
	Button(const char* filePress, const char* fileUnpress, Rect<float>* position, void (*function)(void));
	void display();
	void press(){ isPressed = true;}
	void unpress(){ isPressed = false; func();}
	void (*func)(void);
	Rect<float>* getRect(){ return rect;}
private:
	Texture* pressed;
	Texture* unpressed;
	bool isPressed;
	Rect<float>* rect;
};

#endif
