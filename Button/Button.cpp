#ifndef BUTTON_CPP
#define BUTTON_CPP

#include "Button.h"

Button::Button(const char* filePress, const char* fileUnpress, Rect<float>* position, void (*function)(void)){
	std::cout << "Creating Button\n";
	pressed = new Texture(filePress);
	unpressed = new Texture(fileUnpress);
	rect = position;
	isPressed = false;
	func = function;
}

void Button::display(){
	if(isPressed)
		glBindTexture(GL_TEXTURE_2D, pressed->getTexture());
	else
		glBindTexture(GL_TEXTURE_2D, unpressed->getTexture());
		
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 1.0); glVertex2f(rect->x(), rect->y());
		glTexCoord2f(1.0, 1.0); glVertex2f(rect->x() + rect->width(), rect->y());
		glTexCoord2f(1.0, 0.0); glVertex2f(rect->x() + rect->width(), rect->y() - rect->height());
		glTexCoord2f(0.0, 0.0); glVertex2f(rect->x(), rect->y() - rect->height());
	glEnd();
}

#endif
