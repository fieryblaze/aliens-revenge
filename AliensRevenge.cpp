#include <GL/glut.h>
#include <SOIL/SOIL.h>
#include <iostream>

#include "./HelperFunctions/Texture.cpp"
#include "./Objects/Button.cpp"
#include "./HelperFunctions/Rect.cpp"
#include "./StartMenu.cpp"
#include "./GlutFunctions.cpp"
#include "./Globals.cpp"


//void loadTexture(const char* filename);

static void translate(int x, int y)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(x, y, 0.0f);
}

void blah(void){std::cout << "BLAH!" << std::flush;}

/* Handler for window-repaint event. Call back when the window first appears and
	whenever the window needs to be re-painted. */
void display() {
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	int width = m_viewport[2];
	int height = m_viewport[3];
	//  Ratio of the width to the height of the window
	double w2h = (height>0) ? (double)width/height : 1;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
   
   glColor3f(1,1,1);
   switch(gameState){
   	case START:
   		sMenu->display();
   		break;
   }
   glDisable(GL_TEXTURE_2D);
   
   glFlush();

   glDisable(GL_TEXTURE_2D);
	glutSwapBuffers();
}


int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(640, 640);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("AliensRevenge");
	glutFullScreen();
	glutSetCursor(GLUT_CURSOR_CROSSHAIR);
	glutMouseFunc(mouse);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(0, timer, 0);
	glutSpecialFunc(specialKeypress);
	glutKeyboardFunc(key);
	initGL();
	glutMainLoop();
	return 0;
}
