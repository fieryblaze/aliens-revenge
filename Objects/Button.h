#ifndef BUTTON_H
#define BUTTON_H

#include <iostream>
#include <GL/glut.h>
#include "../HelperFunctions/Texture.cpp"
#include "../HelperFunctions/Rect.cpp"

class Button{
public:
	Button(const char* filePress, const char* fileUnpress, Rect<float>* position, void (*function)(void));
	void display();
	void press(){ isPressed = true;}
	void unpress(){ isPressed = false; func();}
	void (*func)(void);
	Rect<float>* getRect(){ return rect;}
private:
	Texture* pressed;
	Texture* unpressed;
	bool isPressed;
	Rect<float>* rect;
};

#endif
