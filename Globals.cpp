#ifndef GLOBALS_CPP
#define GLOBALS_CPP

#include "./HelperFunctions/Texture.cpp"
#include "./Objects/Button.cpp"
#include "./StartMenu.cpp"

// game states
#define START 0
#define MAIN 1
int gameState = 0;

int refreshMills = 50;
int isPaused = 0;
float camX = 0.0f;
float camY = 0.0f;
float screenWidth = 0;
float screenHeight = 0;

double dim = 50; //dimensions of the projection matrix

#endif
